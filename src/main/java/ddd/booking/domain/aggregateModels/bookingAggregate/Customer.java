package ddd.booking.domain.aggregateModels.bookingAggregate;


public class Customer {

    private long id;
    private String firstName;
    private String lastName;
    private Contract contract;

    public Customer(long id, String firstName, String lastName, Contract contract){
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.contract = contract;
    }

    public Contract getContract() {
        return this.contract;
    }

    public String getFirstName(){
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public long getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "id: " + this.id + " firstName: " + this.firstName + " lastName: " + this.lastName + " contract: "
                + contract.toString();

    }
}
