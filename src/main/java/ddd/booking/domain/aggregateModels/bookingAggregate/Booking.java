package ddd.booking.domain.aggregateModels.bookingAggregate;

public class Booking {
    private long id;
    private RoomDetail roomDetail;
    private Customer customer;

     public Booking(long id, Customer customer, RoomDetail roomDetail) {
        this.id = id;
        this.customer = customer;
        this.roomDetail = roomDetail;
    }

    public RoomDetail getRoomDetail() {
        return roomDetail;
    }

    public void setRoomDetail(RoomDetail roomDetail) {
        this.roomDetail = roomDetail;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    @Override
    public String toString(){
         return "id: " + this.id + " customer: [" + this.customer + "] "+ " bookingDetail: ["+ this.roomDetail +"]";
    }
}
