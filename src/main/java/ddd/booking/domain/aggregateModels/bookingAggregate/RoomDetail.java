package ddd.booking.domain.aggregateModels.bookingAggregate;

public class RoomDetail {
    private int roomNumber;
    private String roomColor;
    private String roomSize;

    public RoomDetail(int roomNumber, String roomColor, String roomSize){
        this.roomNumber = roomNumber;
        this.roomColor = roomColor;
        this.roomSize = roomSize;
    }

    public int getRoomNumber() { return this.roomNumber; }

    public String getRoomColor() {
        return this.roomColor;
    }

    public String getRoomSize() {
        return this.roomSize;
    }

    @Override
    public String toString() {
        return "roomNumber: " + this.roomNumber + " roomSize: " + this.roomSize + " roomColor: " + this.roomColor;
    }
}
