package ddd.booking.domain.aggregateModels.bookingAggregate;

import java.util.List;

public interface BookingRoomRepository {

    public void book(Booking booking);

    public void change(Booking booking);

    public void cancel(long id);

    public <Booking extends List<Booking>> Booking findAll();

    public Booking findByID(long id);
}
