package ddd.booking.domain.aggregateModels.bookingAggregate;

public class Contract {

    private String phoneNumber;

    public Contract(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
         return this.phoneNumber;
    }

    @Override
    public String toString() {
        return "phoneNumber: " + this.phoneNumber;
    }
}
