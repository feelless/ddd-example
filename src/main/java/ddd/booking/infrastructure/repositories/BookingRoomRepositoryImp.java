package ddd.booking.infrastructure.repositories;

import ddd.booking.domain.aggregateModels.bookingAggregate.BookingRoomRepository;

import java.util.List;

import ddd.booking.domain.aggregateModels.bookingAggregate.Booking;

public class BookingRoomRepositoryImp implements BookingRoomRepository {



  @Override
  public void book(Booking booking) {
    System.out.println(booking);
  }

  @Override
  public void change(Booking booking) {

  }

  @Override
  public void cancel(long id) {

  }

  @Override
  public List<Booking> findAll() {
    return null;
  }

  @Override
  public Booking findByID(long id) {
    return null;
  }

}
