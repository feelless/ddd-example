package ddd.booking.application.bookingService;

import ddd.booking.domain.aggregateModels.bookingAggregate.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ddd.booking.infrastructure.repositories.BookingRoomRepositoryImp;

@RestController
class BookingService {

  @GetMapping("/bookingRoom")
  public String booking() {

    BookingRoomRepository br = new BookingRoomRepositoryImp();
    br.book(new Booking(
            1,
            new Customer(1, "gam", "bol", new Contract("1234567")),
            new RoomDetail(1, "red", "4x4")
            )
    );

    return "hello";
  }

}